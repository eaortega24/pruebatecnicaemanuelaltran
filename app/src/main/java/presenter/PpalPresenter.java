package presenter;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.example.epica.pruebaemanuelortega.PrincipalActivity;

import java.util.ArrayList;

import database.BrastlewarkRealm;
import database.RealmIniciador;
import fragment.FragmentLista;
import interfaces.IFragmentLista;
import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import models.Brastlewark;
import utils.Utils;

/**
 * Created by Epica on 19/9/2017.
 */

public class PpalPresenter {

    private IFragmentLista iterator;
    private Realm realm;
    ArrayList<Brastlewark> lista;
    public PpalPresenter(Context frg){

        iterator = ((IFragmentLista) frg) ;
        realm = new RealmIniciador(frg).getRealm();
        lista=new ArrayList<>();

    }

    public void loadMoreData(int desde, int hasta) {
        iterator.setBrastlewarklist(Utils.selectbyrange(realm,desde,hasta));
    }

    public void filter(String criterio){
        RealmResults<BrastlewarkRealm> result;
        try {
            double num = Double.parseDouble(criterio);
            result = realm.where(BrastlewarkRealm.class).lessThanOrEqualTo("age",(int)num).lessThanOrEqualTo("weight",num).or().lessThanOrEqualTo("height",num).findAll();
        } catch (NumberFormatException e) {
            result= realm.where(BrastlewarkRealm.class).contains("name",criterio).or().contains("hairColor",criterio).findAll();
        }

        ArrayList<Brastlewark> retorno = new ArrayList<>();
        Brastlewark brast;
        for(int i=0;i<result.size();i++){
            brast= new Brastlewark();
            brast.setData(result.get(i));
            retorno.add(brast);
        }
        iterator.setBrastlewarkSearch(retorno);
    }

}
