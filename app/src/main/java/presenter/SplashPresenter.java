package presenter;

import android.content.Context;
import android.util.Log;

import java.util.List;

import database.BrastlewarkRealm;
import database.RealmIniciador;
import interfaces.ISplashView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmAsyncTask;
import io.realm.RealmObject;
import io.realm.RealmResults;
import models.Brastlewark;
import models.GnomeModel;
import utils.NetworkService;
import utils.Utils;

/**
 * Created by Epica on 14/9/2017.
 */

//Clase para usar Realm, RxAndroid, y retrofit

public class SplashPresenter {
    private NetworkService service;
    private ISplashView iteractor;
    private Context contexto;
    private Realm realm;
    public SplashPresenter(Context context, NetworkService service){
        contexto=context;
        this.service=service;
       iteractor = (ISplashView)context;
        realm = new RealmIniciador(context).getRealm();

    }

    public void launchGetData(){
        if(realm.isEmpty()){ // no hay datos en la bse de datos entonces los pide online
            loadGnomeList();
        }
        else{ //si esta lleno entonces los consulta
            iteractor.getListQuery( Utils.selectbyrange(realm,0,20));
        }
    }


    private void loadGnomeList(){
        if(Utils.isOnline(contexto)){
            //Patron Observer / Observable , propio de RxAndroid
            Observable<GnomeModel> gnomeObservable =(Observable<GnomeModel>)
                    service.getPreparedObservable(service.getAPI().getGnomeList(),GnomeModel.class,true,true);

            gnomeObservable.subscribeOn(Schedulers.io())// .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe(new Observer<GnomeModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(GnomeModel value) {
                            //cuando tiene exito, guarda los datos descargados en la base de datos de realm
                            RealmAsyncTask as = realm.executeTransactionAsync(realm1 -> {
                                for(int i=0; i<value.getBrastlewark().size();i++){
                                    BrastlewarkRealm brastlewark = realm1.createObject(BrastlewarkRealm.class);

                                    brastlewark.setData(value.getBrastlewark().get(i));
                                    realm1.insert(brastlewark);
                                }
                            }, () -> {

                               iteractor.getListQuery( Utils.selectbyrange(realm,0,20)); //hago un query de los primeros 20 elementos de la db

                            }, error -> {
                                iteractor.showError("Existe algun error que desconocemos :(");
                            });
                        }

                        @Override
                        public void onError(Throwable e) {
                            iteractor.showError("Existe algun error que desconocemos :(");
                        }

                        @Override
                        public void onComplete() {

                        }
                    });

        }else{

        }
    }

    public void close() {
        realm.close();
    }
}
