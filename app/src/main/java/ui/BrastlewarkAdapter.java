package ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.epica.pruebaemanuelortega.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import models.Brastlewark;


public class BrastlewarkAdapter extends RecyclerView.Adapter<BrastlewarkViewHolder>{

    private Context contexto;
    private ArrayList<Brastlewark> lista;

    public BrastlewarkAdapter(Context contexto) {
        this.contexto = contexto;
    }

    public void addData(ArrayList<Brastlewark> l){
        lista = new ArrayList<>();
        lista.addAll(l);
        this.notifyDataSetChanged();
    }

    public void addData(Brastlewark bra){
        lista.add(bra);
        this.notifyDataSetChanged();
    }
    @Override
    public BrastlewarkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_gnome, parent, false);
        return new BrastlewarkViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BrastlewarkViewHolder holder, int position) {
        Brastlewark bras = lista.get(position);
        Picasso.with(contexto)
                .load(bras.getThumbnail())
                .placeholder(R.drawable.progress_animation)
                .error(R.drawable.error)
                .into(holder.getImg_thumbnail());
        String friend,profes;
        friend=profes="";
        if(!bras.getFriends().isEmpty()){
            for(int i=0; i<bras.getFriends().size(); i++){
                friend=friend + bras.getFriends().get(i)+'\n';
            }
        }
        else
            friend=":( a lonely people";

        if(!bras.getProfessions().isEmpty()){
            for(int j=0;j<bras.getProfessions().size();j++){
                profes = profes + bras.getProfessions().get(j)+'\n';
            }
        }
        else
            profes=":( no profession";


        holder.getTxt_name().setText("Nombre: "+bras.getName());
        holder.getAge().setText("Age: "+bras.getAge());
        holder.getWeight().setText("Weight: "+bras.getWeight());
        holder.getHeight().setText("Height: "+bras.getHeight());
        holder.getHair_color().setText("Hair color: "+bras.getHairColor());
        holder.getProfession().setText("Profession: "+profes);
        holder.getFriends().setText("Friends: "+friend);

    }

    @Override
    public int getItemCount() {
        return (null != lista ? lista.size() : 0);
    }
}
