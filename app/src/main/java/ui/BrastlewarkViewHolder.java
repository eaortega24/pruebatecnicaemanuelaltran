package ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.epica.pruebaemanuelortega.R;

/**
 * Created by Epica on 19/9/2017.
 */

public class BrastlewarkViewHolder extends RecyclerView.ViewHolder {
    private ImageView img_thumbnail;
    private TextView txt_name;
    private TextView age;
    private TextView weight;
    private TextView height;
    private TextView hair_color;
    private TextView profession;
    private TextView friends;

    public BrastlewarkViewHolder(View itemView) {
        super(itemView);
        img_thumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
        txt_name = (TextView)itemView.findViewById(R.id.txt_name);
        age = (TextView)itemView.findViewById(R.id.age);
        weight = (TextView)itemView.findViewById(R.id.weight);
        height = (TextView)itemView.findViewById(R.id.height);
        hair_color = (TextView)itemView.findViewById(R.id.hair_color);
        profession = (TextView)itemView.findViewById(R.id.profession);
        friends = (TextView)itemView.findViewById(R.id.friends);
    }

    public ImageView getImg_thumbnail() {
        return img_thumbnail;
    }

    public void setImg_thumbnail(ImageView img_thumbnail) {
        this.img_thumbnail = img_thumbnail;
    }

    public TextView getTxt_name() {
        return txt_name;
    }

    public void setTxt_name(TextView txt_name) {
        this.txt_name = txt_name;
    }

    public TextView getAge() {
        return age;
    }

    public void setAge(TextView age) {
        this.age = age;
    }

    public TextView getWeight() {
        return weight;
    }

    public void setWeight(TextView weight) {
        this.weight = weight;
    }

    public TextView getHeight() {
        return height;
    }

    public void setHeight(TextView height) {
        this.height = height;
    }

    public TextView getHair_color() {
        return hair_color;
    }

    public void setHair_color(TextView hair_color) {
        this.hair_color = hair_color;
    }

    public TextView getProfession() {
        return profession;
    }

    public void setProfession(TextView profession) {
        this.profession = profession;
    }

    public TextView getFriends() {
        return friends;
    }

    public void setFriends(TextView friends) {
        this.friends = friends;
    }
}
