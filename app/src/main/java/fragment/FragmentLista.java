package fragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.epica.pruebaemanuelortega.R;

import java.util.ArrayList;

import interfaces.IFragmentLista;
import models.Brastlewark;
import presenter.PpalPresenter;
import ui.BrastlewarkAdapter;
import ui.EndlessRecyclerViewScrollListener;
import utils.Utils;

/**
 * Created by Epica on 15/9/2017.
 */

public class FragmentLista extends Fragment implements IFragmentLista {
    private View v;
    private String nombrePantalla;
    private RecyclerView rvItems;

    private EndlessRecyclerViewScrollListener scrollListener;
    private BrastlewarkAdapter adapter;
    PpalPresenter presenter;
    ArrayList<Brastlewark> lista;
    ProgressBar loadbar;
    private int desde,hasta;
    Dialog pd;

    public static FragmentLista _construct(ArrayList<Brastlewark> object){
        FragmentLista fragment = new FragmentLista();
        Bundle argumentos = new Bundle();
        argumentos.putSerializable("listainicial",object);
        fragment.setArguments(argumentos);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        desde=0;hasta=20;
        Bundle arguments = getArguments();
        if (arguments != null) {
            lista=(ArrayList<Brastlewark>) arguments.getSerializable("listainicial");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_list, container, false);
        loadbar = (ProgressBar)v.findViewById(R.id.loadbar);
        pd = Utils.get_progress_dialog(this.getActivity());
        setHasOptionsMenu(true);
        adapter= new BrastlewarkAdapter(this.getActivity());
        adapter.addData(lista);
        presenter = new PpalPresenter(getContext());
        rvItems = (RecyclerView) v.findViewById(R.id.recicler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvItems.setLayoutManager(linearLayoutManager);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadbar.setVisibility(View.VISIBLE);
                desde +=20;
                hasta += 20;
                presenter.loadMoreData(desde,hasta);
            }
        };
        // Adds the scroll listener to RecyclerView
        rvItems.setAdapter(adapter);
        rvItems.addOnScrollListener(scrollListener);
        return v;
    }


    @Override
    public void setBrastlewarklist(ArrayList<Brastlewark> brast) {
        for (int i=0;i <brast.size();i++){
            adapter.addData(brast.get(i));
        }
        adapter.notifyDataSetChanged();
        loadbar.setVisibility(View.GONE);
    }

    @Override
    public void setBrastlewarkSearch(ArrayList<Brastlewark> brast) {
     //No hago nada ya que aqui no hay un filtro
    }






    /*@Override
    public boolean onCreateOptionsMenu( Menu menu) {
        getMenuInflater().inflate( R.menu.menu_main, menu);

        MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    adapter.filter("");
                    listView.clearTextFilter();
                } else {
                    adapter.filter(newText);
                }
                return true;
            }
        });

        return true;
    }*/
}
