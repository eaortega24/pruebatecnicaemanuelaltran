package interfaces;

import java.util.ArrayList;

import database.BrastlewarkRealm;
import io.realm.RealmResults;
import models.Brastlewark;
import models.GnomeModel;

/**
 * Created by Epica on 14/9/2017.
 */

public interface ISplashView {
    void showError(String error);
    void getListQuery(ArrayList<Brastlewark> selectbyrange);
}
