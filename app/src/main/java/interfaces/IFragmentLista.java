package interfaces;

import java.util.ArrayList;
import java.util.List;

import models.Brastlewark;

/**
 * Created by Epica on 14/9/2017.
 */

public interface IFragmentLista {

    void setBrastlewarklist(ArrayList<Brastlewark> brast); //envia a la vista el resultado del query
    void setBrastlewarkSearch(ArrayList<Brastlewark> brast); //envia a la vista el resultado del filtro
}
