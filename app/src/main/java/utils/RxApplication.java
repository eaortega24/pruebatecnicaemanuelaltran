package utils;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class RxApplication extends Application {

    private NetworkService networkService;
    @Override
    public void onCreate() {
        super.onCreate();

        networkService = new NetworkService();

    }

    public NetworkService getNetworkService(){
        return networkService;
    }


}

