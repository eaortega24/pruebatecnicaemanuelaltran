package utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.example.epica.pruebaemanuelortega.R;

import java.util.ArrayList;
import java.util.List;

import database.BrastlewarkRealm;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import models.Brastlewark;

/**
 * Created by Epica on 13/9/2017.
 */

public class Utils {

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static ArrayList<Brastlewark>  selectbyrange(Realm realm, int desde, int hasta){

        RealmResults<BrastlewarkRealm> result= realm.where(BrastlewarkRealm.class).between("id",desde,hasta).findAllSorted("id", Sort.ASCENDING);
        ArrayList<Brastlewark> retorno = new ArrayList<>();
        Brastlewark brast;
        for(int i=0;i<result.size();i++){
            brast= new Brastlewark();
            brast.setData(result.get(i));
            retorno.add(brast);
        }

        return retorno;
    }

    public static Dialog get_progress_dialog(Context p_context) {
        try {
            Dialog dialog = new Dialog(p_context, R.style.DialogCustom);
            dialog.setCancelable(false);
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            dialog.addContentView(new ProgressBar(p_context), new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT));

            return dialog;
        } catch (Exception err) {
            return null;
        }
    }
}
