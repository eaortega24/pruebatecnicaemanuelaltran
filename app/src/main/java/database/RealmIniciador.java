package database;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Epica on 14/9/2017.
 */

public class RealmIniciador {
    Realm realm;
    public RealmIniciador(Context context){
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration.Builder().name("gnome.realm").build();
        Realm.setDefaultConfiguration(config);
        realm = Realm.getInstance(config);
    }

    public Realm getRealm() {
        return realm;
    }

    public void setRealm(Realm realm) {
        this.realm = realm;
    }


}
