package database;

import io.realm.RealmObject;

/**
 * Created by Epica on 14/9/2017.
 */

public class RealListaProffesion extends RealmObject {
    private String profession;

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }
}
