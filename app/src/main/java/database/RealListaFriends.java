package database;

import io.realm.RealmObject;

/**
 * Created by Epica on 14/9/2017.
 */

public class RealListaFriends  extends RealmObject {
    private String friend;

    public String getFriend() {
        return friend;
    }

    public void setFriend(String friend) {
        this.friend = friend;
    }
}
