package database;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import models.Brastlewark;

/**
 * Created by Epica on 14/9/2017.
 */

public class BrastlewarkRealm  extends RealmObject{

    private int id;
    private String name;
    private String thumbnail;
    private int age;
    private double weight;
    private double height;
    private String hairColor;
    private RealmList<RealListaProffesion> professions;
    private RealmList<RealListaFriends> friends;

    public BrastlewarkRealm() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public RealmList<RealListaProffesion> getProfessions() {
        return professions;
    }

    public void setProfessions(RealmList<RealListaProffesion> professions) {
        this.professions = professions;
    }

    public RealmList<RealListaFriends> getFriends() {
        return friends;
    }

    public void setFriends(RealmList<RealListaFriends> friends) {
        this.friends = friends;
    }

    public void setData(Brastlewark brastlewark){
        this.id = brastlewark.getId();
        this.name= brastlewark.getName();
        this.thumbnail= brastlewark.getThumbnail();
        this.age= brastlewark.getAge();
        this.weight= brastlewark.getWeight();
        this.height= brastlewark.getHeight();
        this.hairColor= brastlewark.getHairColor();
        RealListaFriends friend;
        for(int i=0;i<brastlewark.getFriends().size();i++){
            friend= new RealListaFriends();
            friend.setFriend(brastlewark.getFriends().get(i));
            this.friends.add(friend);
        }

        RealListaProffesion profesion;
        for(int j=0; j<brastlewark.getProfessions().size(); j++){
            profesion = new RealListaProffesion();
            profesion.setProfession(brastlewark.getProfessions().get(j));
            this.professions.add(profesion);
        }
    }
}
