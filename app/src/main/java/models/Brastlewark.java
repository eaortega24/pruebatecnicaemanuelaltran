
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import database.BrastlewarkRealm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Brastlewark implements Serializable{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("age")
    @Expose
    private int age;
    @SerializedName("weight")
    @Expose
    private double weight;
    @SerializedName("height")
    @Expose
    private double height;
    @SerializedName("hair_color")
    @Expose
    private String hairColor;
    @SerializedName("professions")
    @Expose
    private ArrayList<String> professions = new ArrayList<>();
    @SerializedName("friends")
    @Expose
    private ArrayList<String> friends = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Brastlewark withId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Brastlewark withName(String name) {
        this.name = name;
        return this;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Brastlewark withThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
        return this;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Brastlewark withAge(int age) {
        this.age = age;
        return this;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Brastlewark withWeight(double weight) {
        this.weight = weight;
        return this;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Brastlewark withHeight(double height) {
        this.height = height;
        return this;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public Brastlewark withHairColor(String hairColor) {
        this.hairColor = hairColor;
        return this;
    }

    public ArrayList<String> getProfessions() {
        return professions;
    }

    public void setProfessions(ArrayList<String> professions) {
        this.professions = professions;
    }

    public Brastlewark withProfessions(ArrayList<String> professions) {
        this.professions = professions;
        return this;
    }

    public ArrayList<String> getFriends() {
        return friends;
    }

    public void setFriends(ArrayList<String> friends) {
        this.friends = friends;
    }

    public Brastlewark withFriends(ArrayList<String> friends) {
        this.friends = friends;
        return this;
    }

    public void setData(BrastlewarkRealm brastlewark){
        this.id = brastlewark.getId();
        this.name= brastlewark.getName();
        this.thumbnail= brastlewark.getThumbnail();
        this.age= brastlewark.getAge();
        this.weight= brastlewark.getWeight();
        this.height= brastlewark.getHeight();
        this.hairColor= brastlewark.getHairColor();

        if(brastlewark.getFriends().isEmpty()){
            this.friends = new ArrayList<>();
        }else{

            for(int i=0;i<brastlewark.getFriends().size();i++){

                this.friends.add(brastlewark.getFriends().get(i).getFriend());
            }
        }
        if(brastlewark.getProfessions().isEmpty()){

            this.professions=new ArrayList<>();

        }else{
            for(int j=0; j < brastlewark.getProfessions().size(); j++){

                this.professions.add(brastlewark.getProfessions().get(j).getProfession());
            }
        }
    }

}
