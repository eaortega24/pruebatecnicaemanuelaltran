
package models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class GnomeModel {

    @SerializedName("Brastlewark")
    @Expose
    private List<Brastlewark> brastlewark = null;

    public List<Brastlewark> getBrastlewark() {
        return brastlewark;
    }

    public void setBrastlewark(List<Brastlewark> brastlewark) {
        this.brastlewark = brastlewark;
    }

    public GnomeModel withBrastlewark(List<Brastlewark> brastlewark) {
        this.brastlewark = brastlewark;
        return this;
    }



}
