package com.example.epica.pruebaemanuelortega;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

import interfaces.ISplashView;
import models.Brastlewark;
import presenter.SplashPresenter;
import utils.NetworkService;

public class SpalshActivity extends AppCompatActivity implements ISplashView {
    private NetworkService service;
    SplashPresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        service = new NetworkService(); //clase para unir OkHttp con Retrofit
        presenter=new SplashPresenter(this,service);
        presenter.launchGetData();

    }

    @Override
    public void showError(String error) {
        /*FragmentManager fragmentManager = getSupportFragmentManager();
        DialogoAlerta dialogo = new DialogoAlerta();

        dialogo.show(fragmentManager, error);*/
    }

    @Override
    public void getListQuery(ArrayList<Brastlewark> selectbyrange) {
        Intent intent = new Intent(SpalshActivity.this,PrincipalActivity.class);
        Bundle args = new Bundle();
        args.putSerializable("listainicial",selectbyrange);
        intent.putExtra("bundle",args);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.close();

    }
}
