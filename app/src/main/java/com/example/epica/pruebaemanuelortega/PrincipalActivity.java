package com.example.epica.pruebaemanuelortega;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;

import interfaces.IFragmentLista;
import models.Brastlewark;
import presenter.PpalPresenter;
import ui.BrastlewarkAdapter;
import ui.EndlessRecyclerViewScrollListener;
import utils.Utils;

/**
 * Created by Epica on 15/9/2017.
 */

public class PrincipalActivity extends AppCompatActivity implements IFragmentLista {
    private Toolbar appbar;
    private RecyclerView rvItems;

    private EndlessRecyclerViewScrollListener scrollListener;
    private BrastlewarkAdapter adapter;
    PpalPresenter presenter;
    ArrayList<Brastlewark> lista; //lista con los Brastlewalk
    ProgressBar loadbar;
    private int desde,hasta;
    Dialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ppal);
        desde=0;hasta=20;
        appbar = (Toolbar)findViewById(R.id.appbar);
        setSupportActionBar(appbar);
        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("bundle");
        lista = (ArrayList<Brastlewark>) args.getSerializable("listainicial");



        loadbar = (ProgressBar) findViewById(R.id.loadbar);
        pd = Utils.get_progress_dialog(this);
        adapter= new BrastlewarkAdapter(this);
        adapter.addData(lista);
        presenter = new PpalPresenter(this);
        rvItems = (RecyclerView) findViewById(R.id.recicler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvItems.setLayoutManager(linearLayoutManager);

        //esto escucha el evento cuando el scroll llega al final y carga nuevos datos
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadbar.setVisibility(View.VISIBLE);
                desde +=20;
                hasta += 20;
                presenter.loadMoreData(desde,hasta); //para el MVP, presentador que muestra la data en la vista
            }
        };
        // Adds the scroll listener to RecyclerView
        rvItems.setAdapter(adapter);
        rvItems.addOnScrollListener(scrollListener);

    }
    @Override
    public boolean onCreateOptionsMenu( Menu menu) {
        getMenuInflater().inflate( R.menu.menu_main, menu);

        MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setQueryHint("by name, or hair color, or age, or weight or height");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    lista.clear();
                    adapter.notifyDataSetChanged();
                    rvItems.setAdapter(adapter);

                   presenter.loadMoreData(0,hasta);
                } else {
                    presenter.filter(newText);
                }
                return true;
            }
        });

        return true;
    }

    @Override
    public void onBackPressed() {
        back();

    }
    private void back(){
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        } else {

            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void setBrastlewarklist(ArrayList<Brastlewark> brast) {
        for (int i=0;i <brast.size();i++){
            adapter.addData(brast.get(i));
        }
        adapter.notifyDataSetChanged();
        loadbar.setVisibility(View.GONE);
    }

    @Override
    public void setBrastlewarkSearch(ArrayList<Brastlewark> brast) {
//elimina el adapter actual y lo llena con el resultado del query
        rvItems.setAdapter(null);
        adapter=new BrastlewarkAdapter(getApplication());
        adapter.addData(brast);
        rvItems.setAdapter(adapter);
    }


}
